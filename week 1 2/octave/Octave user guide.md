# Octave user guide

## Matrix

eye, ones, zeros

rand, randn: distribution of Gauss

size, length

magic(n) -> generate a matrix who roll, line and diagonal are all sum up to the same number

### extract

A = [1 2; 3 4; 5 6]	A(3, 2) -> 6	A(3, :)	A(:, 2)	A([1 3], :)->[1 2;5 6]

### append

append a colonne
A = [A, [100; 101; 102]] 	A=[B C] or[B, C]: add colonnes	A = [B; C]: add lines 

### operations:

A .* B : multiple element by element

A .^ 2

1 ./ A

log(v) abs(v) abs([-1; 2; -3])

v + 1 == v + ones(size(v))  

A = (A')' 

A<3 -> return a boolean matrix

prod(a) sum(a) floor(a) ceil(a)

max(A, [], 1) -> max of colonne

max(A, [], 2) -> max of roll

max(max(A))  -> max number

A(:) transfer A to a colonne, max(A(:)) max of all number

inv, pinv(force to inverse)

## System

pwd, ls, cd

## File 

* load fileName.dat, load('filename.dat')

* run sctiptName, run('scriptName ')

*   
* save helle.txt v -ascii:
  save as text(ASCII)

* clear whos who load 

## plot

* hold on : to super plot

* xlabel ylabe

* legend : name for ploted curve

* title

* cd 'path' ; print -dpng 'name.png'

* close: close figure ?

* figure(1); plot(x1,y1); figure(2); plot(x2,y2); subplot(1, 2, 2);

* axis([0. 1 -1 1]): axis range

* imagesc(A): plot figure by valueof matrix

  imagesc(A), colorbar, colormap gray;

  

## Control statement

```octave
for i = 1:10,
	disp(i)
end;

while i <= 5,
	disp(i);
	i++;
end;

while true,
	i = i+1;
	if i == 6,
		break;
	end;
end;
```



## Functions

```octave
%make octave can reach the .m file where you define the function
%use directly the function name
function [y1, y2] = squareThisNumber(x)
y1 = x^2;
y2 = x^3;
```

