# Machine Learning

##  Two interesting definitions:

**Arthur Samuel:** "the field of study that gives computers the ability to learn without being explicitly programmed"

**Tom Mitchell**: "A computer program is said to learn from experience E with respect to some class of tasks T and performance measure P, if its performance at tasks in T, as measured by P, improves with experience E."

## Algorithms Introduction:

- Supervised learning
- Unsupervised learning
- Reinforcement learning
- recommender systems

### Supervised Learning:

* Classification: k nearest neighbored, perceptron,   linear regression, etc.

* Purpose: search for the function h: 
  $$
  h: X\rightarrow Y
  $$ {\lable(efe)}

  * Input and output example: 

  $$
  (x_i, y_i), x_i\in X\subset R^n,y\in Y\subset (-1,1)
  $$

  ​		Where X is the collection of features and Y is the collection of **labels**.

  #### e.g.

  * Regression problem: Supervised Learning the size of different houses in square feet, and price of different houses in  thousands of dollars ("right answers" given, $x_i$ and $y_i$), we may use **Regression**  as **h** to predict the price of a new house. (**continuous** valued output)

    

    We are trying to map input variables to some continuous function.
  
  * classification problem: examine individual customer accounts, and for each account decide if it has been hacked/compromised.(**discrete** valued out put 0 or 1)
  
    We are instead trying to predict results in a discrete output. In other words, we are trying to map input variables into discrete categories.
  
  

### Unsupervised Learning:

* Clustering: clustering, data segment, k-means, association rules, etc.

* Purpose: search for the function f:
  $$
  f : X\rightarrow \{C_1, C_2, ..., C_k\}
  $$
  Where Ci are sets of clusters, and X are the collect of input who **don't have labels**. And we try to find a function f to group or clustering them.

#### e.g.

* Google news cluster today's news from all other news site.

* Group different types of people by DNA micro-array data.

* organize computing clusters, Social network analysis, Market segmentation, Astronomical data analysis.

* Cocktail party problem:

  

  <img src="./cocktailParty.png" style="zoom:50%" />

  ​	Two speakers' voice in these two Microphones have different volume because these microphones are at two different distances from the speakers, each micro record a different combination of these two speaker's voices. And we can separate these two voice with just a simple line of code of the "Cocktail Party Algorithm".

## Some learning algorithm

### 1, Linear regression

#### Introduction

​	A pair of $$(x^{(i)}, y^{(i)})$$, $i^{th}$ element in input x and input y is called a train example,  and the dataset that we will be using is called a a training example.  As a supervised learning algorithm, the purpose is to find the function h so that h(x) is a "good " predictor for the corresponding value of y. For historical reason, this function is called a hypothesis.

For this case: 

​	$$h_{\theta}(x) = \theta_0 x + \theta_1$$

​	by minimizing $$J(\theta_{1}, \theta_{2}) = \frac{1}{2m}\sum_{i=1}^{m}(h_{\theta}(x^{(i)}) - y^{(i)})^2$$

where J is called cost function and is sometimes call **Squared error function** or **Mean squared error** as $J(\theta_0, \theta_1) = \frac{1}{2} \overline{x}$ where $\overline{x}$ is the mean of the square of $h_{\theta}(x_i)-y_i$.

Visualization of a cost function with two parameter $\theta_0, \theta_1$ can be done with:
 * 3D plot: like in In Matlab  or scilab
* Contour plot: A graph that contains many contour lines. A contour line of a two variable function has a constant value at all points of the same line

#### Linear regression with one variable: gradient descent

What we are going to do?
We have some function: 	$J(\theta_0, \theta_1,..., \theta_n)$
we want: 							$min_{\theta_0, \theta_1,..., \theta_n}J(\theta_0, \theta_1,..., \theta_n)$

Gradient descent algorithm:
	start: $(\theta_0, \theta_1,..., \theta_n)$
	repeat until convergence {
		for(j = 0 to n) {
			$\theta_j := \theta_j - \alpha\frac{\partial}{\partial_j}J(\theta_0, \theta_1,.., \theta_n)$
		}
	}
	return $(\theta_0, \theta_1,..., \theta_n)$

